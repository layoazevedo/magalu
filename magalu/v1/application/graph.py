import logging

from flask import jsonify, request
from flask.views import MethodView
from magalu.v1.domain.service.GraphService import GraphService

from webargs.flaskparser import use_kwargs

from marshmallow import fields

logger = logging.getLogger(__name__)


class GraphApi(MethodView):
    graph_args = {'name': fields.String(required=True)}

    def __init__(self):
        self.service = GraphService()

        self.status_code = 200
        self.message = "success"

    def get(self):
        return jsonify(self.service.get_graph())

    @use_kwargs(graph_args)
    def post(self, **kwargs):
        try:
            self.service.is_graph_valid(kwargs, request)
            self.service.save_graph(kwargs, request)

        except Exception as e:
            self.status_code = 400
            self.message = str(e)
            logger.info("Error " + str(e))

        return jsonify({'message': self.message}), self.status_code


class GraphApiByName(MethodView):
    def __init__(self):
        self.service = GraphService()
        self.status_code = 200

    def get(self, name):
        response = {}

        try:
            match = {"friends": list(self.service.get_relationship()[name].keys())}

            if request.args.get == 'full':
                for key in self.service.get_graph().keys():
                    if key not in self.service.get_graph().get:
                        response[key] = self.service.get_graph()[key]

                notMatch = {
                    "not_friends": list(response.keys())
                }

                match.update(notMatch)
            self.message = match

        except KeyError as e:
            self.status_code = 404
            self.message = {'message': "Key not found " + name}
            logger.info("Error " + str(e))

        return jsonify(self.message), self.status_code
