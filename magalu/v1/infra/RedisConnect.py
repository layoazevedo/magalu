import redis
import json


class RedisConnect(object):
    redis = redis.Redis(host='localhost', port=6379, db=0)

    def __init__(self):
        if self.redis.get("graph") is None:
            self.redis.set(
                'graph', json.dumps({
                    "ana": {
                        'carlos': 'carlos',
                        'joao': 'joao',
                        'maria': 'maria',
                        'vinicius': 'vinicius'
                    },
                    "maria": {
                        'ana': 'ana',
                        'vinicius': 'vinicius'
                    },
                    "vinicius": {
                        'ana': 'ana',
                        'maria': 'maria'
                    },
                    "luiza": {
                        "joao": "joao"
                    },
                    "joao": {
                        'luiza': 'luiza',
                        "ana": "ana"
                    },
                    "carlos": {
                        "ana": "ana"
                    }
                })
            )

        if self.redis.get("people") is None:
            self.redis.set(
                'people', json.dumps({
                    "ana": "ana",
                    "maria": "maria",
                    "vinicius": "vinicius",
                    "luiza": "luiza",
                    "joao": "joao",
                    "carlos": "carlos"
                })
            )

    def get_people(self):
        return json.loads(self.redis.get("people"))

    def get_graph(self):
        return json.loads(self.redis.get("graph"))

    def set_keys(self, key, value):
        return self.redis.set(key, value)

    def status(self):
        return self.redis.ping()
