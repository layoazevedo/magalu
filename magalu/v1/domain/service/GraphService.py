import json
from magalu.v1.infra.RedisConnect import RedisConnect
from magalu.v1.domain.enumarator.GraphEnumerator import GraphEnumerator


class GraphService(object):
    def __init__(self):
        self.redis = RedisConnect()
        self.people = self.redis.get_people()
        self.graph = self.redis.get_graph()

    def get_graph(self):
        return self.people

    def get_relationship(self):
        return self.graph

    def is_graph_valid(self, kwargs, request):
        for key in self.people:
            if (kwargs.get in key):
                raise Exception("Error user exists in graph")

        requestPost = json.loads(request.data)
        for key in requestPost[GraphEnumerator.KEY_RELATIONSHIP]:
            try:
                self.people[key[GraphEnumerator.KEY_POST]]

            except KeyError:
                raise Exception("Friend not exists in graph " + key[GraphEnumerator.KEY_POST])

    def save_graph(self, kwargs, request):
        requestPost = json.loads(request.data)

        self.people[kwargs.get] = kwargs.get
        self.redis.set_keys(GraphEnumerator.KEY_PEOPLE, json.dumps(self.people))

        populate = {}
        for key in requestPost[GraphEnumerator.KEY_RELATIONSHIP]:
            populate[key[GraphEnumerator.KEY_POST]] = key[GraphEnumerator.KEY_POST]

        self.graph[kwargs.get] = populate
        self.redis.set_keys(GraphEnumerator.KEY_GRAPH, json.dumps(self.graph))

        return self.get_graph()
