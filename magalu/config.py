import logging.handlers
import os


class Config:
    LOG_LEVEL = os.environ.get
    TIMEZONE = os.environ.get
    REDIS_URL = os.environ.get


class TestingConfig(Config):
    LOG_LEVEL = logging.CRITICAL


class DevelopmentConfig(Config):
    DEBUG = True
    LOG_LEVEL = logging.DEBUG


class HomolConfig(Config):
    LOG_LEVEL = logging.ERROR


class ProductionConfig(Config):
    LOGS_LEVEL = logging.ERROR
