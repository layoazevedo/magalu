from flask import jsonify
from flask.views import MethodView
import json
import os


class SwaggerApi(MethodView):

    def get(self):
        with open(os.getcwd() + "/magalu/views/swagger.json") as f:
            data = json.load(f)

        return jsonify(data)
