import logging

from flask import jsonify
from flask.views import MethodView
from magalu.v1.infra.RedisConnect import RedisConnect


logger = logging.getLogger(__name__)


class HealthcheckApi(MethodView):

    def get(self):
        data = {
            'redis': self.check_redis()
        }

        logger.info('response healthcheck {}'.format(data))
        return jsonify(data)

    def check_redis(self):
        try:
            RedisConnect().get_graph()
            status_redis = 'ok'
        except ConnectionError as e:
            logger.exception(e)
            status_redis = 'fail'

        return status_redis
