from flask.blueprints import Blueprint

from .v1.application.graph import GraphApi, GraphApiByName
from .views.healthcheck import HealthcheckApi
from .views.swagger import SwaggerApi

# generic endpoints
healthcheck_blueprint = Blueprint('healthcheck', __name__, url_prefix='/__health-check')
healthcheck_api = HealthcheckApi.as_view('healthcheck_api')
healthcheck_blueprint.add_url_rule('/', view_func=healthcheck_api, methods=['GET'])

swagger_blueprint = Blueprint('swagger', __name__, url_prefix='/doc')
swagger_api = SwaggerApi.as_view('swagger_api')
swagger_blueprint.add_url_rule('/', view_func=swagger_api, methods=['GET'])

# api v1 endpoints
graph_v1_blueprint = Blueprint('graph', __name__, url_prefix='/v1/graph')
graph_api = GraphApi.as_view('graph_v1')
graph_v1_blueprint.add_url_rule('/', view_func=graph_api, methods=['GET', 'POST'])

graph_v1_by_name_blueprint = Blueprint('graph_by_name', __name__, url_prefix='/v1/graph')
graph_api_by_name = GraphApiByName.as_view('graph_v1')
graph_v1_by_name_blueprint.add_url_rule('/<name>', view_func=graph_api_by_name, methods=['GET'])
