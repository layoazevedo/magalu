import os
import unittest

from flask import Flask, Response, json, jsonify
from flask_redis import FlaskRedis

redis = FlaskRedis()


def generic_api_error(e):
    resp = json.dumps({"error": {
        "code": e.code,
        "message": e.description or e.name
    }})

    response = Response(resp, status=e.code, mimetype='application/json')
    response.cache_control.private = True
    response.cache_control.must_revalidate = True
    return response


def install_error_handlers(error_codes, blueprint):
    for code in error_codes:
        blueprint.errorhandler(code)(generic_api_error)


def create_app(config_var=os.getenv('APPLICATION_ENV', 'Development')):
    app = Flask(__name__)

    app.url_map.strict_slashes = False

    if config_var == 'Testing':
        app.testing = True

    config_name = 'magalu.config.{}Config'.format(config_var)
    app.config.from_object(config_name)

    """redis.init_app(app)"""

    register_blueprints(app)

    error_codes = [400, 401, 403, 404, 405, 406, 408, 409, 410, 412, 415, 422, 428, 429, 500, 501, 504]
    install_error_handlers(error_codes, app)
    register_handlers(app)

    return app


def register_handlers(app):
    @app.errorhandler(422)
    def handle_validation_error(error):
        return jsonify({'errors': error.data['messages']}), 400

    @app.errorhandler(Exception)
    def handle_internal_server_error(error):
        return jsonify({'message': error}), 500


def register_blueprints(app):
    from magalu.routes import healthcheck_blueprint, swagger_blueprint, graph_v1_blueprint, graph_v1_by_name_blueprint

    app.register_blueprint(healthcheck_blueprint)
    app.register_blueprint(swagger_blueprint)
    app.register_blueprint(graph_v1_blueprint)
    app.register_blueprint(graph_v1_by_name_blueprint)


class BaseTestCase(unittest.TestCase):
    maxDiff = None

    def create_app(self):
        app = create_app('Testing')
        app.app_context().push()
        return app

    def setUp(self):
        """ Before each test, set up a blank database """
        self.app = self.create_app()
        self.client = self.app.test_client()
