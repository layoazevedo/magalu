import unittest

from magalu import create_app


class BaseTestCase(unittest.TestCase):

    def create_app(self):
        app = create_app('Testing')
        app.app_context().push()
        return app

    def setUp(self):
        """ Before each test, set up a blank database """
        self.app = self.create_app()
        self.client = self.app.test_client()

        with self.app.app_context():
            self.app.db.drop_all()
            self.app.db.create_all()

    def tearDown(self):
        """ Get rid of the database again after each test. """
        with self.app.app_context():
            self.app.db.drop_all()
            self.app.db.session.rollback()
