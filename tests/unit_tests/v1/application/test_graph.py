import json

from magalu import BaseTestCase


class GraphTestCase(BaseTestCase):

    def test_payload_error(self):
        data = {
            'teste': 'error'
        }

        response = self.client.post('/v1/graph', data=json.dumps(data),
                                    headers={'Content-Type': 'application/json'})

        self.assertEqual(response.status_code, 400)

    def test_check_get_user(self):
        response = self.client.get('/v1/graph')

        self.assertEqual(response.status_code, 200)

    def test_check_get_by_user_name_success(self):
        response = self.client.get('/v1/graph/ana')

        self.assertEqual(response.status_code, 200)

    def test_check_get_graph_relations(self):
        response = self.client.get('/v1/graph/ana?search=full')
        self.assertEqual(response.status_code, 200)

    def test_check_get_by_user_name_not_found(self):
        response = self.client.get('/v1/graph/joaquim')

        self.assertEqual(response.status_code, 404)
