from magalu import BaseTestCase
from magalu.v1.domain.service.GraphService import GraphService
import mock
import unittest


class GraphValueObjectTestCase(BaseTestCase):

    def setUp(self):
        self.graph_service = GraphService()
        self.request = mock.MagicMock()
        self.kwargs = mock.MagicMock()

    def test_validation_user_graph_valid(self):
        self.kwargs.get = "ana"

        with self.assertRaises(Exception):
            self.graph_service.is_graph_valid(self.kwargs, self.request)

        if __name__ == '__main__':
            unittest.main()

    def test_validation_friend_graph_valid(self):
        self.kwargs.get = "layo"
        self.request.data = '{"name" : "marcelo"}'

        with self.assertRaises(KeyError):
            self.graph_service.is_graph_valid(self.kwargs, self.request)

        if __name__ == '__main__':
            unittest.main()

    def test_relationship_valid(self):
        self.assertTrue(self.graph_service.get_relationship())

    def test_graph_valid(self):
        self.assertTrue(self.graph_service.get_graph())
