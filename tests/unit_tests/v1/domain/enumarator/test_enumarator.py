from magalu import BaseTestCase
from magalu.v1.domain.enumarator.GraphEnumerator import GraphEnumerator


class GraphEnumeratorTestCase(BaseTestCase):

    def test_check_list_enumarator(self):
        self.assertEqual("graph", GraphEnumerator.KEY_GRAPH)
        self.assertEqual("people", GraphEnumerator.KEY_PEOPLE)
        self.assertEqual("name", GraphEnumerator.KEY_POST)
        self.assertEqual("friends", GraphEnumerator.KEY_RELATIONSHIP)
