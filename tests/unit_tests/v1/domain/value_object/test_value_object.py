from magalu import BaseTestCase
from magalu.v1.domain.value_object.GraphValueObject import GraphValueObject


class GraphValueObjectTestCase(BaseTestCase):

    def test_value_object_graph(self):
        value_object = GraphValueObject()
        param = "payload name not space"
        self.assertEqual("payloadnamenotspace", value_object.payload(param))
