from magalu import BaseTestCase
from magalu.v1.infra.RedisConnect import RedisConnect


class GraphRedisTestCase(BaseTestCase):

    def test_check_redis_status(self):
        redis = RedisConnect()
        self.assertTrue(redis.status())
