import mock

from magalu import BaseTestCase


class HealthcheckTestCase(BaseTestCase):

    @mock.patch('magalu.views.healthcheck.HealthcheckApi.check_redis')
    def test_check_redis_with_success(self, mock_redis):
        mock_redis.return_value = 'ok'
        response = self.client.get
        self.assertIsNotNone(response)
