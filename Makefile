clean:
	@echo "Execute cleaning ..."
	rm -f *.pyc
	rm -f coverage.xml

pep8:
	@find . -type f -not -path "*./.venv/*" -name "*.py"|xargs flake8 --max-line-length=120 --ignore=E402 --max-complexity=6


tests: clean pep8
	py.test tests -v


tests-with-coverage: clean pep8
	py.test --cov=magalu --cov-report=html tests

sonar: tests-with-coverage
	sonar-scanner
