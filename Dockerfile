FROM python:3.6.2-alpine

MAINTAINER Layo Demetrio <layoazevedo@gmail.com>

LABEL version="1.0.0"

RUN apk add --no-cache --update bash libxslt libxslt-dev libffi-dev linux-headers alpine-sdk build-base

ARG DEPLOY_PATH='/home/deploy/magalu'

RUN mkdir -p $DEPLOY_PATH

ADD magalu $DEPLOY_PATH/magalu
ADD manage.py $DEPLOY_PATH
ADD wsgi.py $DEPLOY_PATH
ADD requirements.txt $DEPLOY_PATH
ADD uwsgi.ini $DEPLOY_PATH

RUN pip install -U pip setuptools
RUN pip install -r $DEPLOY_PATH/requirements.txt

WORKDIR $DEPLOY_PATH

EXPOSE 80

CMD ["newrelic-admin", "run-program", "uwsgi", "--ini", "uwsgi.ini"]
