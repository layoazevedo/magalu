
### Make enviroment ###
```
pyenv shell 3.6.3 &&  python -m venv .venv && source .venv/bin/activate
pip install --upgrade pip setuptools
pip install -r requirements-dev.txt
```
### Running server development ###
```
docker-compose up -d && source .venv/bin/activate
python manage.py runserver
```
### Running unit tests ###
```
make tests
make tests-with-coverage

```
### Documentation swagger ###
```
http://127.0.0.1:5000/doc
Copy return API paste in https://editor.swagger.io/

```

